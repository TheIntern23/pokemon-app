<?php

use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\OAuthController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\UserController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\Settings\PasswordController;
use App\Http\Controllers\Settings\ProfileController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PokemonController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', [LoginController::class, 'logout']);

    Route::get('user', [UserController::class, 'current']);

    Route::patch('settings/profile', [ProfileController::class, 'update']);
    Route::patch('settings/password', [PasswordController::class, 'update']);
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', [LoginController::class, 'login']);
    Route::post('register', [RegisterController::class, 'register']);

    Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail']);
    Route::post('password/reset', [ResetPasswordController::class, 'reset']);

    Route::post('email/verify/{user}', [VerificationController::class, 'verify'])->name('verification.verify');
    Route::post('email/resend', [VerificationController::class, 'resend']);

    Route::post('oauth/{driver}', [OAuthController::class, 'redirect']);
    Route::get('oauth/{driver}/callback', [OAuthController::class, 'handleCallback'])->name('oauth.callback');
});

Route::post('/claimPokemon', [PokemonController::class, 'claimPokemon'])->name('claimPokemon');
Route::get('/fetchPokemonByUser', [PokemonController::class, 'fetchPokemonUser'])->name('fetchPokemonUser'); //Fetch Pokemon to Pokedex
Route::get('/fetchPokemonUserID', [PokemonController::class, 'fetchPokemonUserID'])->name('fetchPokemonUserID'); //Fetch Users ID to Pokedex for trading
Route::delete('/removePokemonByUser/{id}', [PokemonController::class, 'destroy'])->name('destroy'); //Delete Pokemon from Pokedex

//Trade Pokemon functions
Route::put('/tradePokemon/{id}', [PokemonController::class, 'tradePokemon'])->name("tradePokemon"); //Trade pokemon 
Route::get('/fetchPokemonTradeRequests', [PokemonController::class, 'renderPokemonTradeRequests'])->name("renderPokemonTradeRequests"); //Fetches pokemonn trade requests, if any
Route::put('/approveTrade/{id}', [PokemonController::class, 'approveTrade'])->name("approveTrade"); //Approve Trade
Route::put('/rejectTrade/{id}', [PokemonController::class, 'rejectTrade'])->name("rejectTrade"); //Reject Trade