<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        DB::table('claim_status')->insert([

            [
                'status' => 'In Progress'
                
            ],
            [
                'status' => 'Traded',
                
            ],
            [
                'status' => 'Rejected'
                
            ],
           
          
        ]);
        // DB::table('users')->insert([

        //     [
        //         'name' => 'Testing1',
        //         'email' => '1234@gmail.com',
        //         'password' => '123456'
                
        //     ],
        //     [
        //         'name' => 'Testing2',
        //         'email' => '12345@gmail.com',
        //         'password' => '123456'
                
        //     ]
          
           
          
        // ]);
    }
}
