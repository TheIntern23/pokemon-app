<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTradeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trade', function (Blueprint $table) {
            $table->id();
            $table->foreignId('outgoing_user_id')->nullable() //Original pokemon owner
            ->references('id')->on('users')->onDelete('cascade');
            $table->foreignId('incoming_user_id')->nullable() //Future pokemon owner (To be traded with)
            ->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('pokemon_id');
            $table->string('pokemon_name')->nullable(); 
            $table->foreignId('claim_status')->default(1) //User ID linked to claim_pokemon table
            ->references('id')->on('claim_status')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trade');
    }
}
