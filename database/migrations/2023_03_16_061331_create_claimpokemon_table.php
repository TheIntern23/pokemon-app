<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClaimPokemonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claim_pokemon', function (Blueprint $table) {
            $table->id('id');
            $table->foreignId('outgoing_user_id')->nullable() //Original pokemon owner
            ->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('pokemon_id');
            $table->string('pokemon_name')->nullable(); 
            // $table->foreignId('claim_status')->nullable() //Pokemon claim linked to user
            // ->references('id')->on('claim_status')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claim_pokemon');
    }
}
