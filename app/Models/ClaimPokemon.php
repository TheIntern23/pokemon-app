<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClaimPokemon extends Model
{
    protected $table = "claim_pokemon";
    protected  $primaryKey = 'pokemon_id';

    //Remove Pokemon_ID as primary key and prevent
//    /* protected $primaryKey = null;
//     public $incrementing = false;
    protected $fillable = array (

        "pokemon_id",
        "outgoing_user_id",
        'claim_status'  
    );
    use HasFactory;

    //Use User ID to fetch email from users table
    public function user()
     {        
        return $this->belongsTo(User::class, 'outgoing_user_id', 'id');
     }
}
