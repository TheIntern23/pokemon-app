<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trade extends Model
{
    protected $table = "trade";

    protected $fillable = array (
        "outgoing_user_id",
        "incoming_user_id",
        'pokemon_id',
        'pokemon_name',
        'claim_status' 
    );

    //Use User ID to fetch email from users table
    public function user()
     {        
        return $this->belongsTo(User::class, 'incoming_user_id', 'id');
     }
     //Fetches trade status
    public function trade_status()
    {        
       return $this->belongsTo(ClaimStatus::class , 'claim_status', 'id');
    }

    use HasFactory;
}
