<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClaimStatus extends Model
{
    protected $table = "claim_status";
    protected $fillable = [
        'status',
     
    
    ];
    use HasFactory;
}
