<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\ClaimPokemon;
use App\Models\Trade;
use App\Models\ClaimStatus;
use App\Models\User;
use Illuminate\Http\Response;

class PokemonController extends Controller
{
    public function claimPokemon(Request $request)
    {
        $claimPokemon = $request->validate(
            [
                'pokemon_name' => 'required',
            ],
        );
        $isPokemonExists = ClaimPokemon::where('pokemon_name', $request->pokemon_name)->first();
        if (!$isPokemonExists) {
            $id = Auth::id(); //Get the current authenticated user ID
            $claimPokemon = new ClaimPokemon;
            $claimPokemon->pokemon_id = $request->pokemon_id;
            $claimPokemon->pokemon_name = $request->pokemon_name;
            $claimPokemon->outgoing_user_id = $id;
            //$claimPokemon->claim_status = $request->status;
            $claimPokemon->save();
            //For vue rendering
            return response()->json([
                'message' => 'Pokemon Claimed',
                'success' => true
            ]);
        } else {
            //For vue rendering
            return response()->json([
                'message' => 'Pokemon has been claimed by the other users',
                'success' => false
            ]);
        }
    }

    //Fetch pokemon by logged in user in pokedex
    public function fetchPokemonUser(Request $request)
    {
        $user = Auth::user()->id;
        $pokemon = ClaimPokemon::where('claim_pokemon.outgoing_user_id', '=', $user)->get()->pluck('pokemon_name'); //pokemon_id

       // dd($pokemon);

        return response()->json([
            'fetchPokemonByUser' => $pokemon
        ], Response::HTTP_OK);
    }
     //Delete Pokemon by User
     public function destroy($id)
     {
         $pokemon = ClaimPokemon::find($id);
         $pokemon->delete();
 
         return response()->json([
             'status' => 'success'
         ]);
     }
     //Trade Pokemon functions
     //Sents trade request
     public function tradePokemon($id, Request $request)
     {
        //dd($id);
        $id = Auth::id(); //Get the current authenticated user ID
        $tradePokemon = new Trade;
        $tradePokemon->pokemon_id = $request->pokemon_id;
        $tradePokemon->outgoing_user_id = $request->incoming_user_id; //Original pokemon owner 
        $tradePokemon->incoming_user_id = $id; //Future pokemon owner (To be traded with)
        $tradePokemon->pokemon_name = $request->pokemon_name;
        $tradePokemon->save();
  
        //For vue rendering
        return response()->json([
            'message' => 'success'
        ]);

     }
     public function renderPokemonTradeRequests()
     {

        $user = Auth::user()->id;
        $pokemon = Trade::with('user', 'trade_status')->where('trade.incoming_user_id', '!=', $user)->get();
        //dd($pokemon);
        

  
        return response()->json([
            'fetchPokemonTradeRequests' => $pokemon
        ], Response::HTTP_OK);

     }

     public function approveTrade($id, Request $request)
     {
        $approveTradePokemon = Trade::find($id);
        $tradeStatus = ClaimStatus::where('status', '=' , 'Traded')->first();
    
        //dd($tradeStatus);
        
        $temp_outgoing_user = $request->input('outgoing_user_id'); //Logged in user becomes current owner
        $temp_pokemon_name = $request->input('pokemon_name');

        $approveTradePokemon->outgoing_user_id  = $temp_outgoing_user;
        $approveTradePokemon->claim_status = $tradeStatus->id; //Change claim status to traded
        $approveTradePokemon->update();
        

        //Update Pokemon Claimed Table
        $updatePokemonUser = ClaimPokemon::where('pokemon_name', '=', $temp_pokemon_name )->first();
        //$updatePokemonUser = ClaimPokemon::find($temp_outgoing_user);
        $updatePokemonUser->outgoing_user_id = $temp_outgoing_user;
       // dd($updatePokemonUser->outgoing_user_id);
        //$updatePokemonUser->pokemon_name = $temp_pokemon_name;
        $updatePokemonUser->update();

         //For vue rendering
         return response()->json([
            'message' => 'success'
        ]);
      
     }

    public function rejectTrade($id, Request $request)
     {
        $rejectTradePokemon = Trade::find($id);
        $tradeStatus = ClaimStatus::where('status', '=' , 'Rejected')->first();

        //$rejectTradePokemon->outgoing_user_id = $request->input('outgoing_user_id');
        $rejectTradePokemon->claim_status = $tradeStatus->id;
        $rejectTradePokemon->update();

        //Update Pokemon Claimed Table
        // $updatePokemonUser = ClaimPokemon::where('pokemon_name', '=', $temp_pokemon_name )->first();
        // //$updatePokemonUser = ClaimPokemon::find($temp_outgoing_user);
        // $updatePokemonUser->outgoing_user_id = $temp_outgoing_user;
       // dd($updatePokemonUser->outgoing_user_id);


        //For vue rendering
        return response()->json([
        'message' => 'success'
        ]);
        
     }

     //End of trade pokemon functions

     //Fetches all pokemon registered users
     public function fetchPokemonUserID(Request $request)
     {
         $user = Auth::user()->id;
        $pokemon = User::all()->except($user);
 
        // dd($pokemon);
 
         return response()->json([
             'fetchPokemonUserID' => $pokemon
         ], Response::HTTP_OK);

        // $pokemon = ClaimPokemon::where('claim_pokemon.user_id', '=', $user)->get()->pluck('pokemon_id');
        //$pokemon = User::pluck('email', 'id');
       // $pokemon = ClaimPokemon::with('user')->get();
     }
     
}
